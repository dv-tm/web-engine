package com.duskers.web.engine.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duskers.web.engine.base.Message;
import com.duskers.web.engine.entities.Profile;
import com.duskers.web.engine.services.ProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.duskers.web.engine.base.MessageCodes.PROFILE_CREATE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_DELETE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_FIND_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_GET_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_LIST_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_UPDATE_ERROR;
import static com.duskers.web.engine.base.MessageHelper.handle;

@RestController
@RequestMapping(value = "/profiles")
public class ProfileController
{
  private final ProfileService profileService;

  @Autowired
  public ProfileController(ProfileService profileService)
  {
    this.profileService = profileService;
  }

  @GetMapping(value = "/get/{id}")
  public Map<String, Object> get(@PathVariable Long id)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      response.put("profile", profileService.get(id));
    } catch (Exception ex) {
      handle(ex, messages, PROFILE_GET_ERROR);
    }

    return response;
  }

  @PostMapping(value = "/create")
  public Map<String, Object> create(@RequestBody Profile profile)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      profileService.create(profile);
    } catch (Exception ex) {
      handle(ex, messages, PROFILE_CREATE_ERROR);
    }

    return response;
  }

  @PostMapping(value = "/update")
  public Map<String, Object> update(@RequestBody Profile profile)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      profileService.update(profile);
    } catch (Exception ex) {
      handle(ex, messages, PROFILE_UPDATE_ERROR);
    }

    return response;
  }

  @GetMapping(value = "/delete/{id}")
  public Map<String, Object> delete(@PathVariable Long id)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      profileService.delete(id);
    } catch (Exception ex) {
      handle(ex, messages, PROFILE_DELETE_ERROR);
    }

    return response;
  }

  @GetMapping(value = "/list")
  public Map<String, Object> list()
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      response.put("profiles", profileService.list());
    } catch (Exception ex) {
      handle(ex, messages, PROFILE_LIST_ERROR);
    }

    return response;
  }

  @PostMapping(value = "/find")
  public Map<String, Object> find(@RequestBody Profile profile)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      response.put("profiles", profileService.find(profile));
    } catch (Exception ex) {
      handle(ex, messages, PROFILE_FIND_ERROR);
    }

    return response;
  }
}