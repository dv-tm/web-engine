package com.duskers.web.engine.repositories;

import java.util.List;

import com.duskers.web.engine.entities.User;
import com.duskers.web.engine.repositories.base.Repo;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepo extends Repo<User>
{
  @Autowired
  public UserRepo(SessionFactory sessionFactory)
  {
    super(sessionFactory);
  }

  public Class entity()
  {
    return User.class;
  }

  public User get(String login)
  {
    Criteria criteria = this.criteria();
    criteria.add(Restrictions.eq("login", login));
    List<User> users = criteria.list();

    return users.isEmpty() ? null : users.get(0);
  }
}