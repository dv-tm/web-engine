package com.duskers.web.engine.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module.Feature;

/** Jackson Mapper с настройками. */
public class DataMapper extends ObjectMapper
{
  public DataMapper()
  {
    // Регистрация Hibernate модуля для учета его
    // анотаций при построении JSON и XML объектов:
    // LAZY поля пропускаются, а Transient - нет.
    Hibernate5Module module = new Hibernate5Module();
    module.disable(Feature.USE_TRANSIENT_ANNOTATION);

    this.registerModule(module);
  }
}