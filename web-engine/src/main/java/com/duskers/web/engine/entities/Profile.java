package com.duskers.web.engine.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.duskers.web.engine.entities.base.Data;

import org.hibernate.annotations.DynamicUpdate;

import static com.duskers.web.engine.base.MessageCodes.PROFILE_NAME_LENGTH;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_NAME_WRONG;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_PATRONYMIC_LENGTH;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_PATRONYMIC_WRONG;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_SURNAME_LENGTH;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_SURNAME_WRONG;
import static com.duskers.web.engine.base.Patterns.NAME;

@Entity
@DynamicUpdate
@Table(name = "profiles")
public class Profile extends Data
{
  @Size(min = 1, max = 20, message = PROFILE_NAME_LENGTH)
  @Pattern(regexp = NAME, message = PROFILE_NAME_WRONG)
  @Column(name = "name", length = 20)
  private String name;

  @Size(min = 1, max = 20, message = PROFILE_PATRONYMIC_LENGTH)
  @Pattern(regexp = NAME, message = PROFILE_PATRONYMIC_WRONG)
  @Column(name = "patronymic", length = 20)
  private String patronymic;

  @Size(min = 1, max = 30, message = PROFILE_SURNAME_LENGTH)
  @Pattern(regexp = NAME, message = PROFILE_SURNAME_WRONG)
  @Column(name = "surname", length = 30)
  private String surname;

  public Profile() { }

  public Profile(Long id)
  {
    super(id);
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getPatronymic()
  {
    return patronymic;
  }

  public void setPatronymic(String patronymic)
  {
    this.patronymic = patronymic;
  }

  public String getSurname()
  {
    return surname;
  }

  public void setSurname(String surname)
  {
    this.surname = surname;
  }
}