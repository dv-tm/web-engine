package com.duskers.web.engine.base;

import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

public class MessageHelper
{
  private static final Logger logger = LoggerFactory.getLogger(MessageHelper.class);

  /** Обработка ошибок при авторизации. */
  public static Message auth(Exception ex)
  {
    if (ex instanceof BadCredentialsException) {
      return new Message(MessageCodes.AUTH_BAD_CREDENTIALS);
    } else {
      return new Message(MessageCodes.AUTH_UNKNOWN_ERROR);
    }
  }

  /**
   * Обработка ошибки и добавление её сообшений к общему списку.
   *
   * @param ex       Ошибка, которую необходимо обработать.
   * @param messages Список сообщений, который необходимо расширить.
   * @param message  Сообщение, если ошибку не удалось обработать.
   */
  public static void handle(Exception ex, List<Message> messages, String message)
  {
    if (ex instanceof MessageException) {
      messages.addAll(((MessageException) ex).getMessages());
    } else if (ex instanceof ConstraintViolationException) {
      parse((ConstraintViolationException) ex, messages);
    } else {
      logger.debug(message, ex);
      messages.add(new Message(message));
    }
  }

  /**
   * Добавление ошибок валидации к списку сообщений.
   *
   * @param ex       Ошибка валидации, которую необходимо обработать.
   * @param messages Список сообщений, который необходимо расширить.
   */
  private static void parse(ConstraintViolationException ex, List<Message> messages)
  {
    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
      messages.add(new Message(violation.getMessage()));
    }
  }
}