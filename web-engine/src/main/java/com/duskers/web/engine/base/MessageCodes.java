package com.duskers.web.engine.base;

public class MessageCodes
{
  public static final String AUTH_BAD_CREDENTIALS = "AUTH_BAD_CREDENTIALS";
  public static final String AUTH_UNKNOWN_ERROR = "AUTH_UNKNOWN_ERROR";

  public static final String PROFILE_NAME_WRONG = "PROFILE_NAME_WRONG";
  public static final String PROFILE_NAME_LENGTH = "PROFILE_NAME_LENGTH";
  public static final String PROFILE_PATRONYMIC_WRONG = "PROFILE_PATRONYMIC_WRONG";
  public static final String PROFILE_PATRONYMIC_LENGTH = "PROFILE_PATRONYMIC_LENGTH";
  public static final String PROFILE_SURNAME_WRONG = "PROFILE_SURNAME_WRONG";
  public static final String PROFILE_SURNAME_LENGTH = "PROFILE_SURNAME_LENGTH";

  public static final String PROFILE_GET_ERROR = "PROFILE_GET_ERROR";
  public static final String PROFILE_CREATE_ERROR = "PROFILE_CREATE_ERROR";
  public static final String PROFILE_UPDATE_MISSED = "PROFILE_UPDATE_MISSED";
  public static final String PROFILE_UPDATE_ERROR = "PROFILE_UPDATE_ERROR";
  public static final String PROFILE_DELETE_ERROR = "PROFILE_DELETE_ERROR";
  public static final String PROFILE_LIST_ERROR = "PROFILE_LIST_ERROR";
  public static final String PROFILE_FIND_ERROR = "PROFILE_FIND_ERROR";

  public static final String USER_LOGIN_WRONG = "USER_LOGIN_WRONG";
  public static final String USER_LOGIN_LENGTH = "USER_LOGIN_LENGTH";
  public static final String USER_PASSWORD_WRONG = "USER_PASSWORD_WRONG";
  public static final String USER_ALIAS_WRONG = "USER_ALIAS_WRONG";

  public static final String USER_GET_ERROR = "USER_GET_ERROR";
  public static final String USER_CREATE_ERROR = "USER_CREATE_ERROR";
  public static final String USER_ALREADY_EXIST = "USER_ALREADY_EXIST";
  public static final String USER_UPDATE_MISSED = "USER_UPDATE_MISSED";
  public static final String USER_UPDATE_ERROR = "USER_UPDATE_ERROR";
  public static final String USER_DELETE_ERROR = "USER_DELETE_ERROR";
  public static final String USER_LIST_ERROR = "USER_LIST_ERROR";
  public static final String USER_FIND_ERROR = "USER_FIND_ERROR";
}