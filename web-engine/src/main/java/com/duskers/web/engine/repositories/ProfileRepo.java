package com.duskers.web.engine.repositories;

import com.duskers.web.engine.entities.Profile;
import com.duskers.web.engine.repositories.base.Repo;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProfileRepo extends Repo<Profile>
{
  @Autowired
  public ProfileRepo(SessionFactory sessionFactory)
  {
    super(sessionFactory);
  }

  public Class entity()
  {
    return Profile.class;
  }
}