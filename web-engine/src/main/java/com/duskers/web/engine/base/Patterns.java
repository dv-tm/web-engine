package com.duskers.web.engine.base;

public class Patterns
{
  public static final String NAME = "^([A-Za-zА-Яа-яЁё]+)$";
  public static final String EMAIL = "^([A-Za-z0-9_.-]+)@([A-Za-z0-9_.-]+).([A-Za-z.]{2,6})$";
  public static final String PASSWORD = "^([A-Za-z0-9]{8,16})$";
}