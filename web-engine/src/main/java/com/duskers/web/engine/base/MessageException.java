package com.duskers.web.engine.base;

import java.util.ArrayList;

public class MessageException extends Exception
{
  private final ArrayList<Message> messages = new ArrayList<>();

  public MessageException(Message message)
  {
    messages.add(message);
  }

  public MessageException(String code)
  {
    this(new Message(code));
  }

  public void add(Message message)
  {
    messages.add(message);
  }

  public ArrayList<Message> getMessages()
  {
    return messages;
  }
}