package com.duskers.web.engine.services;

import java.util.List;

import com.duskers.web.engine.base.MessageException;
import com.duskers.web.engine.entities.Profile;
import com.duskers.web.engine.entities.User;
import com.duskers.web.engine.repositories.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.duskers.web.engine.base.MessageCodes.USER_ALREADY_EXIST;
import static com.duskers.web.engine.base.MessageCodes.USER_CREATE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_DELETE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_FIND_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_UPDATE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_UPDATE_MISSED;

@Service
@Transactional
public class UserService
{
  private final UserRepo userRepo;
  private final ProfileService profileService;

  @Autowired
  public UserService(UserRepo userRepo, ProfileService profileService)
  {
    this.userRepo = userRepo;
    this.profileService = profileService;
  }

  public User get(Long id)
  {
    return userRepo.get(id);
  }

  public User get(String login)
  {
    return userRepo.get(login);
  }

  public void create(User user) throws MessageException
  {
    if (user == null) {
      throw new MessageException(USER_CREATE_ERROR);
    }

    User existUser = this.get(user.getLogin());

    if (existUser != null) {
      throw new MessageException(USER_ALREADY_EXIST);
    }

    user.setAlias(generateAlias(user));
    user.setPasswordHash(generateHash(user.getPassword()));

    if (user.getProfile() == null) {
      user.setProfile(new Profile());
    }

    if (user.getProfile().getId() == null) {
      profileService.create(user.getProfile());
    } else {
      profileService.update(user.getProfile());
    }

    userRepo.create(user);
  }

  public void update(User user) throws MessageException
  {
    if (user == null || user.getId() == null) {
      throw new MessageException(USER_UPDATE_ERROR);
    }

    User existUser = userRepo.get(user.getId());

    if (existUser == null) {
      throw new MessageException(USER_UPDATE_MISSED);
    }

    if (user.getPassword() != null) {
      user.setPasswordHash(generateHash(user.getPassword()));
    } else {
      user.setPasswordHash(existUser.getPasswordHash());
      existUser.setPassword("12345678"); // Для валидации.
    }

    profileService.update(user.getProfile());

    user.preventUpdate(existUser);
    userRepo.merge(user);
  }

  public void delete(Long id) throws MessageException
  {
    if (id == null) {
      throw new MessageException(USER_DELETE_ERROR);
    }

    userRepo.delete(new User(id));
  }

  public List<User> list()
  {
    return userRepo.list(false);
  }

  public List<User> find(User user) throws MessageException
  {
    if (user == null) {
      throw new MessageException(USER_FIND_ERROR);
    }

    return userRepo.find(user);
  }

  /** Генерация хэша (шифрование) пароля. */
  private static String generateHash(String password) throws MessageException
  {
    if (password != null) {
      return new BCryptPasswordEncoder().encode(password);
    }

    return null;
  }

  /** Генерация псевдонима из логина. */
  private static String generateAlias(User user)
  {
    String alias = user.getAlias();

    if (alias == null || alias.trim().isEmpty()) {
      String login = user.getLogin();

      if (login == null || login.trim().isEmpty()) {
        return null; // Логин и псевдоним пусты.
      } else {
        return login.split("@")[0];
      }
    }

    return alias.trim();
  }
}