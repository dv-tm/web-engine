package com.duskers.web.engine.base;

import java.util.Map;

public class Message
{
  private String code;

  private Map<String, String> params;

  public Message(String code)
  {
    this.code = code;
  }

  public String getCode()
  {
    return code;
  }

  public void setCode(String code)
  {
    this.code = code;
  }

  public Map<String, String> getParams()
  {
    return params;
  }

  public void setParams(Map<String, String> params)
  {
    this.params = params;
  }
}