package com.duskers.web.engine.entities.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** Базовая оболочка для всех сущностей. */
@MappedSuperclass
public class Data implements Serializable
{
  /** Идентификатор сущности в базе данных. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  /** Дата создания сущности в базе данных. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_create", nullable = false, updatable = false)
  private Date dateCreate;

  /** Дата обновления сущности в базе данных. */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false)
  private Date dateUpdate;

  public Data() { }

  public Data(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  @PrePersist
  private void onCreate()
  {
    this.dateCreate = new Date();
    this.dateUpdate = this.dateCreate;
  }

  public Date getDateCreate()
  {
    return dateCreate;
  }

  @PreUpdate
  private void onUpdate()
  {
    this.dateUpdate = new Date();
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  /*
   * Запрос на обновление сущности отправляется в БД даже если изменилась
   * только дата обновления, которая после создания объекта всегда NULL.
   * TODO: Найти автоматический способ предотвращения лишних обновлений.
   */
  public void preventUpdate(Data existData)
  {
    this.dateUpdate = existData.getDateUpdate();
  }
}