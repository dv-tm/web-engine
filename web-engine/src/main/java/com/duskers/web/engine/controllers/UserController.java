package com.duskers.web.engine.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duskers.web.engine.base.Message;
import com.duskers.web.engine.entities.User;
import com.duskers.web.engine.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.duskers.web.engine.base.MessageCodes.USER_CREATE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_DELETE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_FIND_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_GET_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_LIST_ERROR;
import static com.duskers.web.engine.base.MessageCodes.USER_UPDATE_ERROR;
import static com.duskers.web.engine.base.MessageHelper.handle;

@RestController
@RequestMapping(value = "/users")
public class UserController
{
  private final UserService userService;

  @Autowired
  public UserController(UserService userService)
  {
    this.userService = userService;
  }

  @GetMapping(value = "/get/{id}")
  public Map<String, Object> get(@PathVariable Long id)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      response.put("user", userService.get(id));
    } catch (Exception ex) {
      handle(ex, messages, USER_GET_ERROR);
    }

    return response;
  }

  @PostMapping(value = "/create")
  public Map<String, Object> create(@RequestBody User user)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      userService.create(user);
    } catch (Exception ex) {
      handle(ex, messages, USER_CREATE_ERROR);
    }

    return response;
  }

  @PostMapping(value = "/update")
  public Map<String, Object> update(@RequestBody User user)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      userService.update(user);
    } catch (Exception ex) {
      handle(ex, messages, USER_UPDATE_ERROR);
    }

    return response;
  }

  @GetMapping(value = "/delete/{id}")
  public Map<String, Object> delete(@PathVariable Long id)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      userService.delete(id);
    } catch (Exception ex) {
      handle(ex, messages, USER_DELETE_ERROR);
    }

    return response;
  }

  @GetMapping(value = "/list")
  public Map<String, Object> list()
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      response.put("users", userService.list());
    } catch (Exception ex) {
      handle(ex, messages, USER_LIST_ERROR);
    }

    return response;
  }

  @PostMapping(value = "/find")
  public Map<String, Object> find(@RequestBody User user)
  {
    List<Message> messages = new ArrayList<>();
    Map<String, Object> response = new HashMap<>();
    response.put("messages", messages);

    try {
      response.put("users", userService.find(user));
    } catch (Exception ex) {
      handle(ex, messages, USER_FIND_ERROR);
    }

    return response;
  }
}