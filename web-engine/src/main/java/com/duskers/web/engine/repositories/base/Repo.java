package com.duskers.web.engine.repositories.base;

import java.util.List;

import com.duskers.web.engine.entities.base.Data;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public abstract class Repo<T extends Data>
{
  private final SessionFactory sessionFactory;

  @Autowired
  public Repo(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }

  /** Класс поддерживаемой сущности. */
  protected abstract Class entity();

  /** Получение сессии. */
  private Session session()
  {
    return sessionFactory.getCurrentSession();
  }

  /** Создание критериев выборки. */
  protected Criteria criteria()
  {
    return session().createCriteria(this.entity());
  }

  /** Получение объекта. */
  public T get(Long id)
  {
    return (T) session().get(entity(), id);
  }

  /** Получение объекта с прогруженными полями. */
  public T get(Long id, String[] fields)
  {
    Criteria criteria = this.criteria();

    for (String name : fields) {
      criteria.setFetchMode(name, FetchMode.JOIN);
    }

    criteria.add(Restrictions.eq("id", id));
    List<T> objects = criteria.list();

    return objects.isEmpty() ? null : objects.get(0);
  }

  /** Добавление объекта. */
  public void create(T object)
  {
    session().save(object);
  }

  /** Изменение объекта. */
  public void update(T object)
  {
    session().update(object);
  }

  /** Дополнение объекта. */
  public T merge(T object)
  {
    return (T) session().merge(object);
  }

  /** Удаление объекта. */
  public void delete(T object)
  {
    session().delete(object);
  }

  /** Получение списка объектов. */
  public List<T> list(boolean direct, String... orders)
  {
    return this.list(direct, orders, new String[] {});
  }

  /** Получение списка объектов с прогруженными полями. */
  public List<T> list(boolean direct, String[] orders, String[] fields)
  {
    Criteria criteria = this.criteria();

    if (direct) {
      for (String order : orders) {
        criteria.addOrder(Order.asc(order));
      }
    } else {
      if (orders.length == 0) {
        orders = new String[] { "id" };
      }

      for (String order : orders) {
        criteria.addOrder(Order.desc(order));
      }
    }

    for (String name : fields) {
      criteria.setFetchMode(name, FetchMode.JOIN);
    }

    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    return criteria.list();
  }

  /** Поиск объектов. */
  public List<T> find(T object)
  {
    return find(object, new String[] {});
  }

  /** Поиск объектов и прогрузка их полей. */
  public List<T> find(T object, String[] fields)
  {
    Criteria criteria = this.criteria();

    for (String name : fields) {
      criteria.setFetchMode(name, FetchMode.JOIN);
    }

    criteria.add(Example.create(object).excludeZeroes());
    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    return criteria.list();
  }

  /** Получение объекта. */
  public T first(T object)
  {
    return this.first(object, new String[] {});
  }

  /** Получение объекта с прогруженными полями. */
  public T first(T object, String[] fields)
  {
    List<T> objects = this.find(object, fields);

    return objects.isEmpty() ? null : objects.get(0);
  }
}