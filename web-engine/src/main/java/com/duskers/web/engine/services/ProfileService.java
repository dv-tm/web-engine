package com.duskers.web.engine.services;

import java.util.List;

import com.duskers.web.engine.base.MessageException;
import com.duskers.web.engine.entities.Profile;
import com.duskers.web.engine.repositories.ProfileRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.duskers.web.engine.base.MessageCodes.PROFILE_CREATE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_DELETE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_FIND_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_UPDATE_ERROR;
import static com.duskers.web.engine.base.MessageCodes.PROFILE_UPDATE_MISSED;

@Service
@Transactional
public class ProfileService
{
  private final ProfileRepo profileRepo;

  @Autowired
  public ProfileService(ProfileRepo profileRepo)
  {
    this.profileRepo = profileRepo;
  }

  public Profile get(Long id)
  {
    return profileRepo.get(id);
  }

  public void create(Profile profile) throws MessageException
  {
    if (profile == null) {
      throw new MessageException(PROFILE_CREATE_ERROR);
    }

    profileRepo.create(profile);
  }

  public void update(Profile profile) throws MessageException
  {
    if (profile == null || profile.getId() == null) {
      throw new MessageException(PROFILE_UPDATE_ERROR);
    }

    Profile existProfile = profileRepo.get(profile.getId());

    if (existProfile == null) {
      throw new MessageException(PROFILE_UPDATE_MISSED);
    }

    profile.preventUpdate(existProfile);
    profileRepo.merge(profile);
  }

  public void delete(Long id) throws MessageException
  {
    if (id == null) {
      throw new MessageException(PROFILE_DELETE_ERROR);
    }

    profileRepo.delete(new Profile(id));
  }

  public List<Profile> list()
  {
    return profileRepo.list(false);
  }

  public List<Profile> find(Profile profile) throws MessageException
  {
    if (profile == null) {
      throw new MessageException(PROFILE_FIND_ERROR);
    }

    return profileRepo.find(profile);
  }
}