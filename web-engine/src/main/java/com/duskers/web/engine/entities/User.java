package com.duskers.web.engine.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.duskers.web.engine.entities.base.Data;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.DynamicUpdate;

import static com.duskers.web.engine.base.MessageCodes.USER_ALIAS_WRONG;
import static com.duskers.web.engine.base.MessageCodes.USER_LOGIN_LENGTH;
import static com.duskers.web.engine.base.MessageCodes.USER_LOGIN_WRONG;
import static com.duskers.web.engine.base.MessageCodes.USER_PASSWORD_WRONG;
import static com.duskers.web.engine.base.Patterns.EMAIL;
import static com.duskers.web.engine.base.Patterns.PASSWORD;

@Entity
@DynamicUpdate
@Table(name = "users")
public class User extends Data
{
  @NotNull(message = USER_LOGIN_WRONG)
  @Size(min = 3, max = 40, message = USER_LOGIN_LENGTH)
  @Pattern(regexp = EMAIL, message = USER_LOGIN_WRONG)
  @Column(name = "login", length = 40, unique = true, nullable = false, updatable = false)
  private String login;

  @Transient
  @NotNull(message = USER_PASSWORD_WRONG)
  @Pattern(regexp = PASSWORD, message = USER_PASSWORD_WRONG)
  private String password;

  @JsonIgnore
  @Column(name = "password", length = 100, nullable = false)
  private String passwordHash;

  @NotNull(message = USER_ALIAS_WRONG)
  @Size(min = 3, max = 40, message = USER_ALIAS_WRONG)
  @Column(name = "alias", length = 40, nullable = false)
  private String alias;

  @OneToOne
  @JoinColumn(name = "profile", nullable = false, updatable = false)
  private Profile profile;

  public User() { }

  public User(Long id)
  {
    super(id);
  }

  public String getLogin()
  {
    return login;
  }

  public void setLogin(String login)
  {
    this.login = login;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  @JsonIgnore
  public String getPasswordHash()
  {
    return passwordHash;
  }

  public void setPasswordHash(String passwordHash)
  {
    this.passwordHash = passwordHash;
  }

  public String getAlias()
  {
    return alias;
  }

  public void setAlias(String alias)
  {
    this.alias = alias;
  }

  public Profile getProfile()
  {
    return profile;
  }

  public void setProfile(Profile profile)
  {
    this.profile = profile;
  }
}