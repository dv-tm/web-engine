module.exports = (context) => ({
  plugins: {
    'postcss-import': {},
    'postcss-custom-properties': { preserve: false, comments: true },
    'postcss-calc': {},
    'postcss-preset-env': { stage: 0, browsers: ['last 2 versions'] }
  }
});