import React, { PureComponent } from 'react';

export default class MenuItem extends PureComponent
{
  constructor(props)
  {
    super(props);
  }

  active = (event) =>
  {
    const elements = document.getElementsByClassName('ui-menu-item');

    for (let element of elements) {
      element.classList.remove('ui-menu-active');
    }

    event.target.classList.add('ui-menu-active');

    this.props.onClick(event.target.getAttribute('data-action'));
  };

  render()
  {
    return (
      <div className="ui-menu-item"
           data-action={this.props.action}
           onClick={this.active}>
        {this.props.children}
      </div>
    );
  }
}