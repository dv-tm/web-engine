import React, { PureComponent } from 'react';
import '../../styles/menu.pcss';

export default class Menu extends PureComponent
{
  hide()
  {
    document.querySelector('.ui-menu').classList.add('ui-menu-hidden');
    document.querySelector('.ui-menu-overlay').classList.add('ui-menu-overlay-hidden');
  }

  render()
  {
    return (
      <React.Fragment>
        <div className="ui-menu ui-menu-hidden">{this.props.children}</div>
        <div className="ui-menu-overlay ui-menu-overlay-hidden" onClick={this.hide}/>
      </React.Fragment>
    );
  }
}