import React from 'react';

const MenuSeparator = () => (
  <div className="ui-menu-separator"/>
);

export default MenuSeparator;