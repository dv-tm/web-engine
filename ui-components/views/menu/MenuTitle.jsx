import React from 'react';

const MenuTitle = ({ className, children }) => (
  <div className={'ui-menu-title' + (className ? ' ' + className : '')}>{children}</div>
);

export default MenuTitle;