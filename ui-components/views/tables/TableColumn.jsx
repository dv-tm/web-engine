import React from 'react';

const TableColumn = ({ merge, width, children }) => (
  <td colSpan={merge} width={width} className="ui-table-column">{children}</td>
);

export default TableColumn;