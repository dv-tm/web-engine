import React from 'react';
import ButtonIcon from '../controls/ButtonIcon';

const TableColumnButton = ({ merge, type, mask, onClick }) => (
  <td colSpan={merge} className="ui-table-column ui-table-column-button">
    <ButtonIcon type={type} mask={mask} onClick={onClick}/>
  </td>
);

export default TableColumnButton;