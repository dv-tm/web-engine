import React from 'react';

export const TableRow = ({ children }) => (
  <tr className="ui-table-row">{children}</tr>
);

export default TableRow;