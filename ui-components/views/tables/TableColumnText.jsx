import React from 'react';
import Input from '../controls/Input';

const TableColumnText = ({ merge, width, data, name, placeholder, onChange, children }) => (
  <td colSpan={merge} width={width} className="ui-table-column">
    <Input type="text" data={data} name={name} placeholder={placeholder} onChange={onChange} value={children}/>
  </td>
);

export default TableColumnText;