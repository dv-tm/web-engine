import React, { PureComponent } from 'react';
import ButtonIcon from '../controls/ButtonIcon';
import Colors from '../controls/enums/Colors';
import Mask from '../decor/enums/Mask';
import TableColumn from './TableColumn';

const Status = { EDIT: 'EDIT', VIEW: 'VIEW', WAIT: 'WAIT' };

const viewState = {
  updateType: Colors.GREEN, updateMask: Mask.EDIT,
  deleteType: Colors.GRAY, deleteMask: Mask.RECYCLE,
  status: Status.VIEW
};

const editState = {
  updateType: Colors.BLUE, updateMask: Mask.UPDATE,
  deleteType: Colors.RED, deleteMask: Mask.DELETE,
  status: Status.EDIT
};

const updateState = { ...editState, updateMask: Mask.WAIT, status: Status.WAIT };
const deleteState = { ...editState, deleteMask: Mask.WAIT, status: Status.WAIT };

class TableRowEditor extends PureComponent
{
  state = viewState;

  constructor(props)
  {
    super(props);
  }

  update = (event) =>
  {
    if (this.state.status === Status.VIEW) {
      this.setState(editState);
    } else if (this.state.status === Status.EDIT) {
      this.setState(updateState);

      this.props.update(this.props.data, this.updateCallback);
    }
  };

  updateCallback = (success) =>
  {
    if (success) {
      this.setState(viewState);
    } else {
      this.setState(editState);
    }
  };

  delete = () =>
  {
    if (this.state.status === Status.EDIT) {
      this.setState(deleteState);

      this.props.delete(this.props.data, this.deleteCallback);
    }
  };

  deleteCallback = (success) =>
  {
    if (!success) {
      this.setState(editState);
    }
  };

  viewChildren()
  {
    return this.props.children.map((child) =>
    {
      let value;

      if (child.props.data && child.props.name) {
        value = child.props.data[child.props.name];
      } else {
        value = this.props.children;
      }

      if (child.type !== TableColumn) {
        return <td className="ui-table-column ui-table-column-data">{value}</td>;
      }

      return child;
    });
  }

  render()
  {
    const { updateType, updateMask, deleteType, deleteMask, status } = this.state;

    return (
      <tr className="ui-table-row ui-table-row-data">
        <td className="ui-table-column ui-table-column-button">
          <ButtonIcon type={updateType} mask={updateMask} onClick={this.update}/>
        </td>

        {status === Status.VIEW ? this.viewChildren() : this.props.children}

        <td className="ui-table-column ui-table-column-button">
          <ButtonIcon type={deleteType} mask={deleteMask} onClick={this.delete}/>
        </td>
      </tr>
    );
  }
}

export default TableRowEditor;