import React from 'react';
import '../../styles/tables.pcss';

const TableData = ({ children }) => (
  <div className="ui-table-pane">
    <table className="ui-table-data">
      <tbody>{children}</tbody>
    </table>
  </div>
);

export default TableData;