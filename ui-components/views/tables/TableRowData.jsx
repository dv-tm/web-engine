import React from 'react';

export const TableRowData = ({ children }) => (
  <tr className="ui-table-row ui-table-row-data">{children}</tr>
);

export default TableRowData;