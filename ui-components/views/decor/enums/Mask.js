const Mask = Object.freeze({
  CREATE: 'create',
  DELETE: 'delete',
  EDIT: 'edit',
  FIND: 'find',
  LOGIN: 'login',
  MENU: 'menu',
  RECYCLE: 'recycle',
  REFRESH: 'refresh',
  UPDATE: 'update',
  WAIT: 'wait',

  define(mask)
  {
    if (typeof mask === 'string') {
      mask = mask.toUpperCase();
    }

    return this[mask] || this.WAIT;
  }
});

export default Mask;