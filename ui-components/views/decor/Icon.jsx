import React from 'react';
import '../../styles/icons.pcss';
import Mask from './enums/Mask';
import Icons from './Icons';

const Icon = ({ mask, className }) =>
{
  mask = Mask.define(mask);
  className = (className ? ' ' + className : '');

  return (
    <span className={'ui-icon ui-icon-' + mask + className}>
      <svg width="24" height="24" viewBox="0 0 24 24">{Icons[mask]}</svg>
    </span>
  );
};

export default Icon;