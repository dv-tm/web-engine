import React from 'react';
import '../../styles/buttons.pcss';
import Colors from '../controls/enums/Colors';
import Icon from '../decor/Icon';

const ButtonIcon = ({ type, mask, onClick, children }) => (
  <button className={'ui-button ui-button-' + Colors.define(type)} onClick={onClick}>
    <Icon className="ui-button-item" mask={mask}/>
    {children && (<span className="ui-button-item">{children}</span>)}
  </button>
);

export default ButtonIcon;