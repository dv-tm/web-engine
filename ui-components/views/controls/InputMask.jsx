import React from 'react';
import '../../styles/inputs.pcss';

const Input = ({ type, mask, name, placeholder, onChange, value = '' }) => (
  <input type={type}
         name={name}
         placeholder={placeholder}
         className="ui-input ui-input-mask"
         onChange={onChange}
         defaultValue={value}/>
);

export default Input;