import { PureComponent } from 'react';
import React from 'react';
import '../../styles/inputs.pcss';

class Input extends PureComponent
{
  #value;

  constructor(props)
  {
    super(props);

    if (this.props.data && this.props.name) {
      this.#value = this.props.data[this.props.name];
    } else {
      this.#value = this.props.value;
    }
  }

  onChange = (event) =>
  {
    if (this.props.data && this.props.name) {
      this.props.data[this.props.name] = event.target.value;
    }

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(event);
    }
  };

  render()
  {

    return (
      <input type={this.props.type}
             name={this.props.name}
             placeholder={this.props.placeholder}
             className="ui-input"
             onChange={this.onChange}
             defaultValue={this.#value}/>
    );
  }
}

export default Input;