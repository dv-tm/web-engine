const Colors = Object.freeze({
  BLUE: 'blue',
  GREEN: 'green',
  RED: 'red',
  GRAY: 'gray',
  WHITE: 'white',

  define(type)
  {
    if (typeof type === 'string') {
      type = type.toUpperCase();
    }

    return this[type] || this.BLUE;
  }
});

export default Colors;