import React from 'react';
import '../../styles/buttons.pcss';
import Colors from '../controls/enums/Colors';

const Button = ({ type, onClick, children }) => (
  <button className={'ui-button ui-button-' + Colors.define(type)} onClick={onClick}>
    {children}
  </button>
);

export default Button;