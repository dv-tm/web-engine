import React from 'react';
import '../../styles/bar.pcss';

const Bar = ({ children }) => (
  <div className={'ui-bar'}>{children}</div>
);

export default Bar;