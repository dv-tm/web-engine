import React from 'react';
import Icon from '../decor/Icon';
import BarItem from './BarItem';

const BarButton = ({ float, mask, onClick }) => (
  <BarItem float={float} className="ui-bar-button" onClick={onClick}>
    <Icon mask={mask}/>
  </BarItem>
);

export default BarButton;