import React from 'react';
import BarItem from './BarItem';

const BarTitle = ({ float, children }) => (
  <BarItem float={float} className="ui-bar-title">{children}</BarItem>
);

export default BarTitle;