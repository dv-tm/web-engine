import React from 'react';
import BarItem from './BarItem';

const BarLink = ({ float, onClick, children }) => (
  <BarItem float={float} className="ui-bar-link" onClick={onClick}>{children}</BarItem>
);

export default BarLink;