import React from 'react';

const BarItem = ({ float, className, onClick, children }) =>
{
  className = 'ui-bar-item '
    + (float === 'right' ? 'ui-bar-right' : 'ui-bar-left')
    + (className ? ' ' + className : '');

  return (
    <div className={className} onClick={onClick}>{children}</div>
  );
};

export default BarItem;