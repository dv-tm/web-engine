import React, { PureComponent } from 'react';
import Mask from '../decor/enums/Mask';
import BarButton from './BarButton';

export default class BarButtonMenu extends PureComponent
{
  show()
  {

  }

  hide()
  {

  }

  popup()
  {
    document.querySelector('.ui-menu-overlay').classList.remove('ui-menu-overlay-hidden');
    document.querySelector('.ui-menu').classList.remove('ui-menu-hidden');
  }

  render()
  {
    return (
      <BarButton mask={Mask.MENU} onClick={this.popup}/>
    );
  }
}