import React from 'react';
import '../../styles/copyright.pcss';

const Copyright = ({ children }) => (
  <div className="ui-copyright">{children} </div>
);

export default Copyright;