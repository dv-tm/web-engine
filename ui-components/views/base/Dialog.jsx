import React from 'react';
import '../../styles/dialog.pcss';

const Dialog = ({ children }) => (
  <div className={'ui-dialog-overlay'}>
    <div className={'ui-dialog'}>{children}</div>
  </div>
);

export default Dialog;