import React from 'react';

import '../../styles/basic.pcss';
import '../../styles/fonts.pcss';

const App = ({ className = '', children }) => (
  <div className={className}>{children}</div>
);

export default App;