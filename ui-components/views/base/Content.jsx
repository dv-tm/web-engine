import React from 'react';
import '../../styles/content.pcss';

const Content = ({ children }) => (
  <div className={'ui-content'}>{children}</div>
);

export default Content;