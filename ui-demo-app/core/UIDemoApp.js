import React from 'react';
import ReactDOM from 'react-dom';
import DemoApp from '../views/DemoApp.jsx';

class UIDemoApp
{
    #selector;

    constructor(params)
    {
        params || (params = {});

        this.#selector = params.selector;
        this.render();
    }

    render()
    {
        ReactDOM.render(<DemoApp/>, document.querySelector(this.#selector));
    }
}

export function create(selector)
{
    return new UIDemoApp({ selector });
}