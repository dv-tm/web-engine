const path = require('path');
const { name, library, version } = require('./package.json');

module.exports = function (env, args)
{
  const development = env && env.development;

  const configuration = {
    mode: development ? 'development' : 'production',

    entry: {
      'core': './core/' + library
    },

    output: {
      path: path.resolve('./scripts'),
      filename: name + '.[name].js',
      chunkFilename: './scripts/' + name + '.[name].js',
      library: library,
      libraryTarget: 'umd',
      umdNamedDefine: true
    },

    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        '@ui-components': path.resolve('./vendors/ui-components-1.0.0.js')
      }
    },

    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: [
            path.resolve('./core'),
            path.resolve('./vendors'),
            path.resolve('./views')
          ],
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-react']
              ],
              plugins: [
                ['@babel/plugin-syntax-dynamic-import'],                // Поддержка динамического импорта.
                ['@babel/plugin-proposal-export-default-from'],         // Поддержка импорта по умолчанию.
                ['@babel/plugin-proposal-class-properties'],            // Поддержка приватных полей классов.
                ['@babel/plugin-proposal-object-rest-spread'],          // Поддержка REST, SPREAD конструкций.
                ['@babel/plugin-transform-classes']                     // Преобразование классов в функции.
              ]
            }
          }
        },
        {
          test: /\.pcss$/,
          include: [
            path.resolve('./styles')
          ],
          loader: 'style-loader!css-loader!postcss-loader'
        },
        {
          test: /\.(png|jpg|svg)$/,
          include: [
            path.resolve('./icons'),
            path.resolve('./images')
          ],
          loader: 'url-loader'
        },
        {
          test: /\.(woff2|woff|ttf|eot|svg)$/,
          include: [
            path.resolve('./fonts')
          ],
          loader: 'file-loader?name=fonts/[name].[ext]'
        }
      ]
    }
  };

  if (development) {
    configuration.devtool = 'cheap-module-source-map';
  }

  return configuration;
};