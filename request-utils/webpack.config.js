const path = require('path');
const { name, library, version } = require('./package.json');

module.exports = function (env, args)
{
  const development = env && env.development;

  const configuration = {
    mode: development ? 'development' : 'production',

    entry: {
      'core': './core/' + library
    },

    output: {
      path: path.resolve('./target'),
      filename: name + '-' + version + '.js',
      library: library,
      libraryTarget: 'umd',
      umdNamedDefine: true
    },

    resolve: {
      extensions: ['.js']
    },

    module: {
      rules: [
        {
          test: /\.(js)$/,
          include: [
            path.resolve('./core'),
            path.resolve('./entities'),
            path.resolve('./utils')
          ],
          use: {
            loader: 'babel-loader',
            options: {
              plugins: [
                ['@babel/plugin-proposal-export-default-from'],
                ['@babel/plugin-proposal-class-properties'],
                ['@babel/plugin-proposal-object-rest-spread']
              ]
            }
          }
        }
      ]
    }
  };

  if (development) {
    configuration.devtool = 'cheap-module-source-map';
  }

  return configuration;
};