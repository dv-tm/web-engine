import FormRequest from './FormRequest';

export default class JsonRequest extends FormRequest
{
  formatParams(params = {})
  {
    return JSON.stringify(params);
  }

  formatHeaders(headers = {})
  {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      ...headers
    };
  }
}