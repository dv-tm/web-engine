import Templates from './Templates';

export default class Message
{
  #code; // Код сообщения.
  #params; // Параметры сообщения.

  constructor(code, params)
  {
    this.#code = code;
    this.#params = params || {};
  }

  get code()
  {
    return this.#code;
  }

  get params()
  {
    return this.#params;
  }

  text()
  {
    let text = Templates.define(this.#code);

    for (const key in this.#params) {
      // Замена {экранированных} наименований параметров их значениями.
      text = text.replace('{' + key + '}', this.#params[key]);
    }

    return text;
  }
}