import Codes from './Codes';
import Message from './Message';
import RequestType from './RequestType';
import RespFormat from './RespFormat';

const {
  REQUEST_HEADERS_ERROR,
  RESPONSE_PARSE_ERROR,
  RESPONSE_UNKNOWN_ERROR,
  HTTP_400_BAD_REQUEST,
  HTTP_401_UNAUTHORIZED,
  HTTP_403_FORBIDDEN,
  HTTP_404_NOT_FOUND,
  HTTP_405_METHOD_NOT_ALLOWED,
  HTTP_500_INTERNAL_ERROR,
  MESSAGE_CODE_FORMAT_WRONG,
  MESSAGE_LIST_FORMAT_WRONG
} = Codes;

export default class FormRequest
{
  #URL;
  #requestType;
  #respFormat;

  constructor(URL, requestType, respFormat)
  {
    this.#URL = URL;
    this.#requestType = RequestType.define(requestType);
    this.#respFormat = RespFormat.define(respFormat);
  }

  get URL()
  {
    return this.#URL;
  }

  get requestType()
  {
    return this.#requestType;
  }

  get respFormat()
  {
    return this.#respFormat;
  }

  formatURL(params = {})
  {
    let URL = this.#URL;

    for (const key in params) {
      const param = params[key];
      const partURL = '{' + key + '}';

      URL = URL.replace(partURL, param);
    }

    return URL;
  }

  formatParams(params = {})
  {
    let paramsHasData = false;
    const data = new FormData();

    for (const key in params) {
      const param = params[key];
      const partURL = '{' + key + '}';

      if (this.#URL.indexOf(partURL) < 0) {
        if (Array.isArray(param)) {
          for (const item of param) {
            data.append(key, item);
          }
        } else {
          data.append(key, param);
        }

        paramsHasData = true;
      }
    }

    return paramsHasData ? data : null;
  }

  formatHeaders(headers = {})
  {
    return { ...headers };
  }

  run(params = {}, headers = {})
  {
    const URL = this.formatURL(params);
    headers = this.formatHeaders(headers);
    params = this.formatParams(params);

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            const format = this.#respFormat;

            try {
              if (format === RespFormat.JSON) {
                resolve(parse(xhr.responseText));
              } else if (format === RespFormat.SCRIPT) {
                resolve(eval(xhr.responseText));
              } else {
                resolve(xhr.responseText);
              }
            } catch (ex) {
              reject(errors(xhr, RESPONSE_PARSE_ERROR, { format }));
            }

          } else if (xhr.status === 400) {
            reject(errors(xhr, HTTP_400_BAD_REQUEST));
          } else if (xhr.status === 401) {
            reject(errors(xhr, HTTP_401_UNAUTHORIZED));
          } else if (xhr.status === 403) {
            reject(errors(xhr, HTTP_403_FORBIDDEN));
          } else if (xhr.status === 404) {
            reject(errors(xhr, HTTP_404_NOT_FOUND));
          } else if (xhr.status === 405) {
            reject(errors(xhr, HTTP_405_METHOD_NOT_ALLOWED));
          } else if (xhr.status === 500) {
            reject(errors(xhr, HTTP_500_INTERNAL_ERROR));
          } else {
            reject(errors(xhr, RESPONSE_UNKNOWN_ERROR));
          }
        } else {
          // TODO: Что делать с остальными состояниями?
        }
      };

      xhr.open(this.#requestType, URL);
      xhr.withCredentials = true;

      for (const key in headers) {
        let header = headers[key];
        if (typeof key === 'string' && typeof header === 'string') {
          xhr.setRequestHeader(key, header);
        } else {
          reject(errors(xhr, REQUEST_HEADERS_ERROR));
        }
      }

      params ? xhr.send(params) : xhr.send();
    });
  }
}

/**
 * Получение JSON объекта из ответа сервера.
 * @param {String} response - Ответ сервера.
 */
function parse(response)
{
  if (response) {
    response = JSON.parse(response);
    wrapMessagesToClasses(response);

    return response;
  }

  return {};
}

/**
 * Приведение сообщений ответа к классу Message.
 * @param {Object|String} response - Ответ сервера.
 */
function wrapMessagesToClasses(response)
{
  const messages = [];

  if (response.messages) {
    if (Array.isArray(response.messages)) {
      for (const message of response.messages) {
        if (typeof message.code === 'string') {
          messages.push(new Message(message.code, message.params));
        } else {
          messages.push(new Message(MESSAGE_CODE_FORMAT_WRONG));
        }
      }
    } else {
      messages.push(new Message(MESSAGE_LIST_FORMAT_WRONG));
    }
  }

  response.messages = messages;
}

/**
 * Генерация объекта ответа сервера при ошибках.
 * @param {XMLHttpRequest} xhr - Объект запроса.
 * @param {String} code - Строковый код ошибки.
 * @param {Object} params - Набор параметров.
 */
function errors(xhr, code, params = undefined)
{
  const headersRaw = xhr.getAllResponseHeaders();
  const headersLines = headersRaw.trim().split(/[\r\n]+/);

  const headers = {};

  for (const header of headersLines) {
    const parts = header.split(': ');
    const key = parts.shift().toLowerCase();
    headers[key] = parts.join(': ');
  }

  return {
    headers,
    status: xhr.status,
    messages: [
      new Message(code, params)
    ]
  };
}