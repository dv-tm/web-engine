import FormRequest from './FormRequest';
import RequestType from "./RequestType";

export default class GetRequest extends FormRequest
{
  constructor(URL, respFormat)
  {
    super(URL, RequestType.GET, respFormat);
  }

  formatParams(params = {})
  {
    return null;
  }
}