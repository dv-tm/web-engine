export default class RequestType
{
  static get GET() { return 'GET'; }
  static get POST() { return 'POST'; }
  static get DELETE() { return 'DELETE'; }
  static get PUT() { return 'PUT'; }

  static define(type)
  {
    if (typeof type === 'string') {
      type = type.toUpperCase();
    }

    return this[type] || this.POST;
  }
}