import Codes from './Codes';

const baseTemplates = { // Базовые обязательные сообщения.
  [Codes.REQUEST_HEADERS_ERROR]: 'Неверный формат заголовков запроса.',
  [Codes.REQUEST_PARAMS_ERROR]: 'Неверный формат параметров запроса.',

  [Codes.RESPONSE_PARSE_ERROR]: 'Ошибка преобразования ответа к {format} формату.',
  [Codes.RESPONSE_UNKNOWN_ERROR]: 'Неизвестная ошибка при получении ответа.',

  [Codes.HTTP_400_BAD_REQUEST]: 'Неверные параметры запроса.',
  [Codes.HTTP_401_UNAUTHORIZED]: 'Необходима авторизация.',
  [Codes.HTTP_403_FORBIDDEN]: 'Доступ запрещен.',
  [Codes.HTTP_404_NOT_FOUND]: 'Адрес не найден.',
  [Codes.HTTP_405_METHOD_NOT_ALLOWED]: 'Метод не поддерживается.',
  [Codes.HTTP_500_INTERNAL_ERROR]: 'Внутренняя ошибка сервера.',

  [Codes.MESSAGE_CODE_FORMAT_WRONG]: 'Код сообщения не соответствует стандарту.',
  [Codes.MESSAGE_LIST_FORMAT_WRONG]: 'Список сообщений не соответствует стандарту.',
  [Codes.MESSAGE_TEMPLATE_NOT_FOUND]: 'Шаблон сообщения для кода не определен.',

  [Codes.UNKNOWN_ERROR]: 'Неизвестная ошибка.'
};

let list = baseTemplates;

export default class Templates
{
  static define(code)
  {
    if (typeof code === 'string') {
      code = code.toUpperCase();
    }

    return list[code] || list.MESSAGE_TEMPLATE_NOT_FOUND;
  }

  static add(templates)
  {
    if (templates && typeof templates === 'object') {
      list = { ...list, ...templates };
    }
  }
}