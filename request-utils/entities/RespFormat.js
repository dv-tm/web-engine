export default class RespFormat
{
  static get JSON() { return 'JSON'; }
  static get XML() { return 'XML'; }
  static get SCRIPT() { return 'SCRIPT'; }
  static get HTML() { return 'HTML'; }

  static define(format)
  {
    if (typeof format === 'string') {
      format = format.toUpperCase();
    }

    return this[format] || this.JSON;
  }
}