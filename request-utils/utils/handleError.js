import Codes from '../entities/Codes';
import Message from '../entities/Message';

/**
 * Приведение неизвестных ошибок к классу сообщения.
 * @param {Message|Object|String} object - Объект ошибки.
 * @return {Message} message - Обработанное сообщение.
 */
export default function handleError(object)
{
  if (object instanceof Message) {
    return object;
  } else {
    return new Message(Codes.UNKNOWN_ERROR);
  }
}